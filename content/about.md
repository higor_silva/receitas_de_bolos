---
title: "About"
date: 2023-09-27T16:24:51-03:00
draft: false
---

O bolo surgiu no Egito Antigo e era feito a partir de pães adoçados com xaropes de frutas, tâmaras e passas. No período renascentista a receita foi melhorada pelos romanos, que conheciam a técnica da fermentação. Foram eles que colocaram o nome bolo, por causa do formato redondo, vindo de bola.


CHOCOLATE
Já o chocolate surgiu na América Pré-Colombiana. Antes da colonização, os maias cultivavam cacau e extraíam um líquido amargo de suas sementes. Eles misturavam baunilha e pimenta à bebida, chamada de xocoaltl, que tomavam para combater o cansaço.

BOLO + CHOCOLATE
Até o século XVII o chocolate era servido somente na forma de bebida. A partir daí os confeiteiros ingleses começaram a estudar uma forma diferente de consumir o chocolate. Foi quando em 1674 eles tiveram a idéia de misturar cacau às misturas de bolos e servir a receita em empórios.